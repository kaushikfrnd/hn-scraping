#!/usr/bin/python
# Author kaushik gandhi
# email kaushikfrnd@gmail.com
import sys, traceback
import MySQLdb
import config
import json
import urllib2
#----------------------- Class for hacker news integration ----------------------------

class HN_Integration(object):
    """This class is to scrape the whole HN posts and its user base """
    def __init__(self):
        try :
            self.db = MySQLdb.connect(config.db_config['SERVER'], # your host, usually localhost
                     config.db_config['USERNAME'], # your username
                      config.db_config['PASSWORD'], # your password
                      config.db_config['NAME']) # name of the data base
            self.cursor=self.db.cursor()
        except:
            print "Database connection error"
    #----------------------------detructor------------------------------
    def __del__(self):
          self.db.close()

#----------------------------------------get front page news------------------------------------
    def get_news(self):
      while True:
        try :
        #print 'starting' , config.appspot_url+'news'
          news_json=json.load(urllib2.urlopen(config.news_url))
          return news_json["results"]
          break
        except:
          print "exception in feeding posts"
          traceback.print_exc(file=sys.stdout)
#-----------------------------------------get comments------------------------------------------------

    def flatten_comments(self,node, acc = []):
      acc += [node['items']]
      if 'children' in node:
        for child in node['children']:
            self.flatten_comments(child, acc)

    def get_comments(self,post_id):
      while True:
        try :
        #print 'starting' , config.appspot_url+'news'
          news_json=json.load(urllib2.urlopen(config.comments_url+post_id))
          #print news_json
          return news_json["results"]
          break
        except:
          print "exception in feeding posts"
          traceback.print_exc(file=sys.stdout)

    def feed_comments(self,post_id):
      comments=self.get_comments(post_id)
      for comment in comments:
        try:
          self.cursor.execute('''INSERT into COMMENTS (id ,discussion_id ,discussion_sigid ,parent_id ,username,comment_text,comment_time,karma_points ) 
            values (%s,%s,%s,%s,%s,%s,%s,%s)''',
            (comment['item']['_id'],comment['item']['discussion']['id'],comment['item']['discussion']['sigid'],comment['item']['parent_id'],comment['item']['username'],comment['item']['text'],comment['item']['create_ts'],comment['item']['points']))
          self.db.commit()
          self.feed_user(comment['item']['username'])

      #print "feeded user" , user_id
        except:
          print "exception caught"
          traceback.print_exc(file=sys.stdout)

#------------------------------------------------feed user in database------------------------------------

    def feed_user(self,user_id) :
      try:
       user = self.get_user(user_id) 
       self.cursor.execute('''INSERT into USERINFO (id ,karma_points ,created_utc ,username ,flag,about ) 
          values (%s,%s,%s,%s,%s,%s)''',
         (user['_id'],user['karma'],user['create_ts'],user['username'],"false",user['about']))
       self.db.commit()
      #print "feeded user" , user_id
      except:
        print "exception caught"
        traceback.print_exc(file=sys.stdout)

    def get_user(self,user_id):
      try:
        user_details=json.load(urllib2.urlopen(config.user_details_url+user_id))
        return user_details
      except:
        print 'exception in fetching user'
#----------------------------------------------feed posts in database------------------------------------------
    def feed_posts(self):
      try:
        posts=self.get_news()
        for post in posts:
          self.cursor.execute('''INSERT into POSTS (id,title,permalink,karma_points,url,author)
              values (%s,%s,%s,%s,%s,%s)''',(post['item']['_id'],post['item']['title'],'https://news.ycombinator.com/item?id='+str(post['item']['id']),post['item']['points'],post['item']['url'],post['item']['username']))
          self.db.commit()
          self.feed_user(post['item']['username'])
          self.feed_comments(post['item']['_id'])
      except:
        print 'exception occurred'
        traceback.print_exc(file=sys.stdout)

#---------------------------------------------feed users,comments,posts in database starting from posts-----------------------------------------------
    def get_submitted(self,username):
      try:
        user_posts=json.load(urllib2.urlopen(config.user_posts_url+username))
        print user_posts
        return user_posts["results"]
      except:
        print 'exception in fetching user'
        traceback.print_exc(file=sys.stdout)


    def feed_by_user(self) :
     while True:
        try :
         rows_count=self.cursor.execute('''SELECT username from USERINFO WHERE flag=%s''',('false'))
         if rows_count > 0:
            results = self.cursor.fetchall()
         else :
           break
         for user_name in results:
           print user_name[0] , 'in progress'
           posts = self.get_submitted(user_name[0])
           print posts
           for post in posts :
              self.cursor.execute('''INSERT into POSTS (id,title,permalink,karma_points,url,author)
                values (%s,%s,%s,%s,%s,%s)''',(post['item']['_id'],post['item']['title'],'https://news.ycombinator.com/item?id='+str(post['item']['id']),post['item']['points'],post['item']['url'],post['item']['username']))
              self.db.commit()
              self.cursor.execute('''UPDATE USERINFO SET flag=%s
              WHERE username=%s''',('true',post['item']['username']))
              self.db.commit()
              self.feed_comments(post['item']['_id'])
              self.feed_user(post['item']['username'])
        
        except:
          print "exception caught" 
          traceback.print_exc(file=sys.stdout)


hni=HN_Integration()
#hni.feed_posts()
hni.feed_by_user()


    
    
